# Exoscale-GitLab-Kubernetes Demo

This project contains a set of Kubernetes manifests to install MariaDB, WordPress, and the K8up backup operator, to schedule a backup every 2 minutes.
