#!/usr/bin/env bash

exo compute sks kubeconfig sks-cluster user -z ch-gva-2 --group system:masters > exoscale-sks.kubeconfig

function k () {
    kubectl --kubeconfig exoscale-sks.kubeconfig "$@"
}

KUBERNETES_URL=$(k cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')
echo ""
echo "••• URL •••"
echo "$KUBERNETES_URL"

KUBERNETES_CERTIFICATE=$(k get secret $(k get secrets | grep default | awk '{ print $1 }') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
echo ""
echo "••• Certificate •••"
echo "$KUBERNETES_CERTIFICATE"

echo ""
echo "••• Creating system account •••"
k apply -f gitlab/service-account.yaml

SYSTEM_ACCOUNT_TOKEN=$(k -n kube-system get secret -o jsonpath="{['data']['token']}" $(k -n kube-system get secret | grep gitlab | awk '{print $1}') | base64 --decode)
echo ""
echo "••• System account token •••"
echo "$SYSTEM_ACCOUNT_TOKEN"
