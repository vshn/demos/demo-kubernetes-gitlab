export KUBECONFIG="./exoscale-sks.kubeconfig"
export RESTIC_REPOSITORY=s3:https://sos-ch-gva-2.exo.io/backups-getting-started-repo/
export RESTIC_PASSWORD=p@ssw0rd
export AWS_ACCESS_KEY_ID=$EXOSCALE_API_KEY
export AWS_SECRET_ACCESS_KEY=$EXOSCALE_API_SECRET
