#!/usr/bin/env bash

export KUBECONFIG="./exoscale-sks.kubeconfig"

echo ""
echo "••• Installing Longhorn storage controller •••"
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.3.1/deploy/longhorn.yaml

echo ""
echo "••• Installing Secrets •••"
echo -n $EXOSCALE_API_KEY > ./username.txt
echo -n $EXOSCALE_API_SECRET > ./password.txt
kubectl create secret generic backup-credentials --from-file=./username.txt  --from-file=./password.txt
rm ./username.txt
rm ./password.txt

echo ""
echo "••• Installing K8up •••"
kubectl apply -f https://github.com/k8up-io/k8up/releases/download/v2.4.0/k8up-crd.yaml
helm install k8up appuio/k8up --namespace k8up-operator --create-namespace

echo ""
echo "••• Done! Use kubectl or k9s to use your cluster •••"
